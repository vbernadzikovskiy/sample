import 'package:create_user/screens/create_user/screens/create_user_screen.dart';
import 'package:create_user/utils/constants/colors.dart';
import 'package:create_user/utils/constants/strings.dart';
import 'package:create_user/utils/screen_names.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(MyHomePage());
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    var _systemTheme = SystemUiOverlayStyle.dark.copyWith(
        systemNavigationBarColor: AppColors.ff0f223d,
        systemNavigationBarIconBrightness: Brightness.light,
        statusBarIconBrightness: Brightness.light,
        statusBarBrightness: Brightness.light,
        statusBarColor: AppColors.ff0f223d);

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.landscapeLeft,
      DeviceOrientation.landscapeRight,
      DeviceOrientation.portraitDown,
    ]);

    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: _systemTheme,
        child: MaterialApp(
            title: AppStrings.createUser,
            darkTheme:
                ThemeData(brightness: Brightness.light, fontFamily: "Lato"),
            theme: ThemeData(brightness: Brightness.light, fontFamily: "Lato"),
            initialRoute: ScreenNames.initial,
            routes: {
              ScreenNames.initial: (context) => CreateUserScreen(),
            }));
  }
}

import 'package:flutter/material.dart';
import 'package:create_user/utils/constants/colors.dart';

class CustomButton extends StatelessWidget {
  final String text;
  final Color textColor;
  final Color color;
  final VoidCallback onPressed;

  const CustomButton({
    Key key,
    @required this.text,
    this.textColor = Colors.white,
    this.color = AppColors.ff1aa2f0,
    @required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            color: color, borderRadius: BorderRadius.circular(18.0)),
        child: MaterialButton(
            minWidth: 0,
            padding: EdgeInsets.fromLTRB(16, 5, 16, 7),
            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
            child: _buildContent(),
            onPressed: onPressed,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(18.0))));
  }

  Widget _buildContent() {
    return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(text, textAlign: TextAlign.center, style: textStyle()),
        ]);
  }

  TextStyle textStyle() =>
      TextStyle(color: textColor, fontWeight: FontWeight.bold, fontSize: 16);
}

import 'package:flutter/material.dart';
import 'package:create_user/utils/constants/colors.dart';

class CustomTextField extends StatefulWidget {
  CustomTextField(
      {this.labelText,
      this.hintText,
      this.imageIcon,
      this.onChanged,
      this.controller,
      this.isPassword = false});

  final String labelText;
  final String hintText;
  final String imageIcon;
  final TextEditingController controller;

  final ValueChanged<String> onChanged;
  bool isPassword;

  @override
  _CustomTextFieldState createState() => new _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  FocusNode _focus = FocusNode();
  bool _isFocused = false;

  @override
  void initState() {
    super.initState();
    _focus.addListener(_onFocusChanged);
  }

  void _onFocusChanged() {
    setState(() {
      _isFocused = !_isFocused;
    });
  }

  Widget _setupImage() {
    if (widget.imageIcon != null) {
      return ClipOval(
        child: Image.asset(widget.imageIcon,
            fit: BoxFit.contain, width: 18, height: 20),
      );
    }

    return Container();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: _isFocused ? AppColors.ff143255 : AppColors.ff0f223d,
      margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
      padding: EdgeInsets.fromLTRB(24, 10, 0, 0),
      height: 85,
      child: Container(
        decoration: BoxDecoration(
          border: Border(
              bottom: BorderSide(
                  width: 1,
                  color: _isFocused
                      ? AppColors.disabledCell
                      : AppColors.ff193b62)),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              widget.labelText,
              style: TextStyle(
                  color: _isFocused ? Colors.white : AppColors.disabledCell,
                  fontFamily: 'Lato',
                  fontSize: 14),
            ),
            SizedBox(height: 4),
            Row(
              children: <Widget>[
                Expanded(
                  child: TextField(
                    obscureText: widget.isPassword,
                    onChanged: widget.onChanged,
                    autocorrect: false,
                    focusNode: _focus,
                    maxLines: 1,
                    autofocus: false,
                    cursorColor: AppColors.ff1aa2f0,
                    controller: widget.controller,
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      color: Colors.white,
                      fontSize: 16,
                    ),
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      errorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                      hintStyle: TextStyle(
                          fontWeight: FontWeight.w500,
                          color: AppColors.disabledDark,
                          fontSize: 16),
                      isDense: true,
                      contentPadding: EdgeInsets.fromLTRB(0, 4, 0, 4),
                      hintText: _isFocused ? "" : widget.hintText,
                    ),
                  ),
                ),
                SizedBox(
                  width: 18,
                  height: 20,
                ),
                _setupImage(),
                SizedBox(
                  width: 24,
                  height: 20,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

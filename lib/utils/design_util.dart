import 'package:flutter/material.dart';

import 'device_util.dart';

const EdgeInsets _kTabletPortraitEdgeInsets =
    EdgeInsets.fromLTRB(213, 0, 213, 0);
const EdgeInsets _kTabletLandscapeEdgeInsets =
    EdgeInsets.fromLTRB(352, 0, 352, 0);
const EdgeInsets _kPhonePortraitEdgeInsets = EdgeInsets.zero;
const EdgeInsets _kPhoneLandscapeEdgeInsets =
    EdgeInsets.fromLTRB(30, 0, 10, 30);

const double _kTabletPortraitContinueButtonRightInset = 249;
const double _kTabletLandscapeContinueButtonRightInset = 249;
const double _kPhonePortraitContinueButtonRightInset = 200;
const double _kPhoneLandscapeContinueButtonRightInset = 200;

class DesignUtil {
  static const double kAppBarPreferredHeight = 70;

  static EdgeInsets getCurrentInsets() {
    switch (DeviceUtil.deviceType) {
      case DeviceType.mobile:
        if (DeviceUtil.deviceOrientation == Orientation.portrait)
          return _kPhonePortraitEdgeInsets;
        return _kPhoneLandscapeEdgeInsets;
      case DeviceType.tablet:
        if (DeviceUtil.deviceOrientation == Orientation.portrait)
          return _kTabletPortraitEdgeInsets;
        return _kTabletLandscapeEdgeInsets;
    }

    return EdgeInsets.zero;
  }

  static double getButtonRightInset() {
    switch (DeviceUtil.deviceType) {
      case DeviceType.mobile:
        if (DeviceUtil.deviceOrientation == Orientation.portrait)
          return _kPhonePortraitContinueButtonRightInset;
        return _kPhoneLandscapeContinueButtonRightInset;
      case DeviceType.tablet:
        if (DeviceUtil.deviceOrientation == Orientation.portrait)
          return _kTabletPortraitContinueButtonRightInset;
        return _kTabletLandscapeContinueButtonRightInset;
    }

    return 0;
  }
}

class AppStrings {
  static const String createUser = 'Create User';
  static const String fullName = 'Full Name';
  static const String fullNameHint = 'e.g John Smith';
  static const String email = 'Email';
  static const String emailHint = 'name@mail.com';
  static const String password = 'Password';
  static const String confirmPassword = 'Confirm Password';
  static const String continueText = 'Continue';
}

import 'package:flutter/material.dart';

class AppColors {
  static const ff0f223d = Color(0xFF0f223d);
  static const ff143255 = Color(0xFF143255);
  static const ff193b62 = Color(0xFF193b62);
  static const ff4a6485 = Color(0xFF4a6485);
  static const ff1a94e1 = Color(0xFF1a94e1);
  static const ff1aa2f0 = Color(0xFF1aa2f0);
  static const ff92a2b6 = Color(0xff92a2b6);
  static const ff1fcf85 = Color(0xFF1fcf85);
  static const ff6a7e99 = Color(0xFF6a7e99);
  static const disabledDark = Color.fromRGBO(255, 255, 255, 0.38);
  static const disabledCell = Color.fromRGBO(255, 255, 255, 0.6);
}

import 'package:flutter/material.dart';

const double _kMaximumMobileScreenWidth = 600;

class DeviceUtil {

  static BuildContext context;

  static DeviceType get deviceType {
    if (MediaQuery.of(context).size.shortestSide > _kMaximumMobileScreenWidth) return DeviceType.tablet;
    return DeviceType.mobile;
  }

  static Orientation get deviceOrientation {
      return MediaQuery.of(context).orientation;
  }
}

enum DeviceType {
  mobile, tablet
}

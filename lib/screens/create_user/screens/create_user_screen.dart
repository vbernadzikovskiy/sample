import 'package:create_user/utils/constants/colors.dart';
import 'package:create_user/utils/constants/strings.dart';
import 'package:create_user/utils/design_util.dart';
import 'package:create_user/utils/device_util.dart';
import 'package:create_user/utils/widgets/custom_button.dart';
import 'package:create_user/utils/widgets/custom_text_field.dart';
import 'package:flutter/material.dart';

class CreateUserScreen extends StatefulWidget {
  @override
  _CreateUserScreenState createState() => _CreateUserScreenState();
}

class _CreateUserScreenState extends State<CreateUserScreen> {
  @override
  void initState() {
    super.initState();
  }

  TextEditingController _fullNameController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _confirmPasswordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    DeviceUtil.context = context;

    return Scaffold(
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(DesignUtil.kAppBarPreferredHeight),
            child: AppBar(
              backgroundColor: AppColors.ff0f223d,
              title: Text(AppStrings.createUser),
            )),
        backgroundColor: AppColors.ff0f223d,
        body: _buildContent());
  }

  Widget _buildContent() {
    return CustomScrollView(
      slivers: <Widget>[
        SliverPadding(
            padding: DesignUtil.getCurrentInsets(),
            sliver: SliverList(
                delegate: SliverChildListDelegate([
              SizedBox(
                height: 20,
              ),
              CustomTextField(
                labelText: AppStrings.fullName,
                hintText: AppStrings.fullNameHint,
                controller: _fullNameController,
              ),
              CustomTextField(
                labelText: AppStrings.email,
                hintText: AppStrings.emailHint,
                controller: _emailController,
              ),
              CustomTextField(
                labelText: AppStrings.password,
                isPassword: true,
                controller: _passwordController,
              ),
              CustomTextField(
                labelText: AppStrings.confirmPassword,
                isPassword: true,
                controller: _confirmPasswordController,
              ),
              Padding(
                  padding: EdgeInsets.fromLTRB(
                      24, 25, DesignUtil.getButtonRightInset(), 0),
                  child: CustomButton(
                      text: AppStrings.continueText,
                      onPressed: () {
                        print(_fullNameController.text);
                        print(_emailController.text);
                        print(_passwordController.text);
                        print(_confirmPasswordController.text);
                      }))
            ])))
      ],
    );
  }
}
